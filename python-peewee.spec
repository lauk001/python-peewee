%global _empty_manifest_terminate_build 0
Name:		python-peewee
Version:	3.15.1
Release:	1
Summary:	a little orm
License:	MIT
URL:		https://github.com/coleifer/peewee/
Source0:        https://files.pythonhosted.org/packages/45/c4/be139f7b7e0bbbc7b2fba4dc492cfb0202c64a0086fa2d23c0b6091ef4f2/peewee-3.15.1.tar.gz
BuildArch:	noarch


%description
Peewee is a simple and small ORM. It has few (but expressive) concepts, making it easy to learn and intuitive to use.

%package -n python3-peewee
Summary:	a little orm
Provides:	python-peewee
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-peewee
Peewee is a simple and small ORM. It has few (but expressive) concepts, making it easy to learn and intuitive to use.

%package help
Summary:	Development documents and examples for peewee
Provides:	python3-peewee-doc
%description help
Peewee is a simple and small ORM. It has few (but expressive) concepts, making it easy to learn and intuitive to use.

%prep
%autosetup -n peewee-3.15.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-peewee -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 05 2022 liukuo <liukuo@kylinos.cn> - 3.15.1-1
- Update to 3.15.1

* Thu Dec 16 2021 Python_Bot <Python_Bot@openeuler.org> - 3.14.8-1
- Package Init
